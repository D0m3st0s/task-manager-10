package ru.shumov.tm.command;


import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Constants;
import ru.shumov.tm.enums.Role;

public class TaskClearCommand extends AbstractCommand {
    private final Role role = Role.ADMINISTRATOR;
    @Getter
    private final String name = "task clear";
    @Getter
    private final String description = "task clear: Удаление всех задач.";

    @Override
    public void execute() {
        @Nullable final var user = bootstrap.getUser();
        if (user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if (user.getRole().equals(role)) {
            bootstrap.getTerminalService().outPutString(Constants.ALL_TASKS_WILL_BE_CLEARED);
            bootstrap.getTerminalService().outPutString(Constants.YES_NO);
            @NotNull final var answer = bootstrap.getTerminalService().scanner();
            if (Constants.YES.equals(answer)) {
                bootstrap.getTaskService().clear();
            }
        } else {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
        }
    }

    public TaskClearCommand() {
    }
}
