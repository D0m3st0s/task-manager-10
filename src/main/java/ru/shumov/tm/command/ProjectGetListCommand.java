package ru.shumov.tm.command;

import lombok.Getter;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Constants;
import ru.shumov.tm.entity.Project;
import ru.shumov.tm.enums.Role;


import java.text.SimpleDateFormat;
import java.util.Collection;

public class ProjectGetListCommand extends AbstractCommand {
    private final Role role = Role.USER;
    @Getter
    private final String name = "project list";
    @Getter
    private final String description = "project list: Вывод всех проектов.";
    @Override
    public void execute() {
        int counter = 0;
        @Nullable final var user = bootstrap.getUser();
        if (user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if (user.getRole().equals(role)) {
            @Nullable Collection<Project> values = bootstrap.getProjectService().getList();
            if (values.isEmpty()) {
                bootstrap.getTerminalService().outPutString(Constants.PROJECTS_DO_NOT_EXIST);
                return;
            }
            for (Project project : values) {
                if (project.getUserId().equals(bootstrap.getUser().getId())) {
                    counter++;
                }
            }
            if (counter == 0) {
                bootstrap.getTerminalService().outPutString(Constants.PROJECTS_DO_NOT_EXIST);
                return;
            }
            for (Project project : values) {
                if (project.getUserId().equals(bootstrap.getUser().getId())) {
                    bootstrap.getTerminalService()
                            .outPutString(bootstrap.getToStringService().projectToString(project));
                }
            }
        } else {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
        }
    }

    public ProjectGetListCommand() {
    }
}
