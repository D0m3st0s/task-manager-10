package ru.shumov.tm.command;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Constants;
import ru.shumov.tm.entity.Project;
import ru.shumov.tm.enums.Role;
import ru.shumov.tm.enums.Status;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class ProjectUpdateCommand extends AbstractCommand {
    private final Role role = Role.USER;
    @Getter
    private final String name = "project update";
    @Getter
    private final String description = "project update: Изменение параметров проекта.";

    @Override
    public void execute() {
        @Nullable final var user = bootstrap.getUser();
        var project = new Project();
        if (user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if (user.getRole().equals(role)) {
            var format = new SimpleDateFormat();
            format.applyPattern("dd.MM.yyyy");
            bootstrap.getTerminalService().outPutString(Constants.ENTER_ID_OF_PROJECT_FOR_SHOWING);
            @NotNull final var projectId = bootstrap.getTerminalService().scanner();
            if (!bootstrap.getProjectService().checkKey(projectId)) {
                bootstrap.getTerminalService().outPutString(Constants.PROJECT_DOES_NOT_EXIST);
                return;
            }
            if (bootstrap.getProjectService().checkKey(projectId)) {
                project = bootstrap.getProjectService().getOne(projectId);
            }
            if (!project.getUserId().equals(user.getId())) {
                bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS_FOR_PROJECT);
                return;
            }
            try {
                bootstrap.getTerminalService().outPutString(Constants.ENTER_PROJECT_NAME);
                @NotNull final var name = bootstrap.getTerminalService().scanner();
                bootstrap.getTerminalService().outPutString(Constants.ENTER_START_DATE_OF_PROJECT);
                @NotNull final var startDate = bootstrap.getTerminalService().scanner();
                bootstrap.getTerminalService().outPutString(Constants.ENTER_DEADLINE_OF_PROJECT);
                @NotNull final var endDate = bootstrap.getTerminalService().scanner();
                bootstrap.getTerminalService().outPutString(Constants.ENTER_DESCRIPTION_OF_PROJECT);
                @NotNull final var description = bootstrap.getTerminalService().scanner();
                bootstrap.getTerminalService().outPutString("Введите статус проекта:");
                bootstrap.getTerminalService().outPutString("planned/in process/done");
                @NotNull final var status = bootstrap.getTerminalService().scanner();
                switch (status) {
                    case ("planned"):
                        project.setStatus(Status.PLANNED);
                        break;
                    case ("in process"):
                        project.setStatus(Status.IN_PROCESS);
                        break;
                    case ("done"):
                        project.setStatus(Status.DONE);
                        break;
                }

                project.setName(name);
                project.setDescription(description);
                project.setStartDate(format.parse(startDate));
                project.setEndDate(format.parse(endDate));
                bootstrap.getProjectService().update(project);
            } catch (ParseException parseException) {
                bootstrap.getTerminalService().outPutString(Constants.INCORRECT_DATE_FORMAT);
            }
        } else {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
        }
    }

    public ProjectUpdateCommand() {
    }
}
