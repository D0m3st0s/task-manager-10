package ru.shumov.tm.command;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class DownloadCommand extends AbstractCommand{
    private String name = "load";
    private String description = "load: Загрузка сохраненных проектов и задач.";
    @Override
    public void execute() {
        bootstrap.getTerminalService().outPutString("В каком формате вы хотите загрузить данные?");
        bootstrap.getTerminalService().outPutString("ser java || jaxb xml || jaxb json || fas xml || fas json");
        final var answer = bootstrap.getTerminalService().scanner();
        switch (answer) {
            case "ser java":
                bootstrap.getUnConversionService().unSerialization();
                break;
            case "jaxb xml":
                bootstrap.getUnConversionService().jaxbXmlUnConvertation();
                break;
            case "jaxb json":
                bootstrap.getUnConversionService().jaxbJsonUnConvertation();
                break;
            case "fas xml":
                bootstrap.getUnConversionService().fasterXmlUnConvertation();
                break;
            case "fas json":
                bootstrap.getUnConversionService().fasterJsonUnConvertation();
                break;
        }
    }
}
