package ru.shumov.tm.command;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class SaveCommand extends AbstractCommand{
    private String name = "save";
    private String description = "save: Сохранение проектов и задач.";
    @Override
    public void execute() {
        bootstrap.getTerminalService().outPutString("В каком формате вы хотите сохранить данные?");
        bootstrap.getTerminalService().outPutString("ser java || jaxb xml || jaxb json || fas xml || fas json");
        final var answer = bootstrap.getTerminalService().scanner();
        switch (answer) {
            case "ser java":
                bootstrap.getConversionService().serialization();
                break;
            case "jaxb xml":
                bootstrap.getConversionService().jaxbXmlConvertation();
                break;
            case "jaxb json":
                bootstrap.getConversionService().jaxbJsonConvertation();
                break;
            case "fas xml":
                bootstrap.getConversionService().fasterXmlConvertation();
                break;
            case "fas json":
                bootstrap.getConversionService().fasterJsonConvertation();
                break;
        }
    }
}
