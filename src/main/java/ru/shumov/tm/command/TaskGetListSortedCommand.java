package ru.shumov.tm.command;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Constants;
import ru.shumov.tm.entity.Task;
import ru.shumov.tm.enums.Role;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public class TaskGetListSortedCommand extends AbstractCommand {
    private final Role role = Role.USER;
    @Getter
    private final String name = "task list sorted";
    @Getter
    private final String description = "task list sorted: Вывод всех задач с сортировкой.";

    @Override
    public void execute() throws NoSuchAlgorithmException {
        List<Task> tasks = new ArrayList<>();
        var counter = 0;
        @Nullable final var user = bootstrap.getUser();
        if (user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if (user.getRole().equals(role)) {
            @Nullable Collection<Task> values = bootstrap.getTaskService().getList();
            if (values.isEmpty()) {
                bootstrap.getTerminalService().outPutString(Constants.TASKS_DO_NOT_EXIST);
                return;
            }
            for (Task task : values) {
                if (task.getUserId().equals(bootstrap.getUser().getId())) {
                    counter++;
                    tasks.add(task);
                }
            }
            if (counter == 0) {
                bootstrap.getTerminalService().outPutString(Constants.TASKS_DO_NOT_EXIST);
                return;
            }
            bootstrap.getTerminalService().outPutString("Как вы хотите отсортировать проекты:");
            bootstrap.getTerminalService().outPutString("by creating date/by start date/by end date/by status");
            var method = bootstrap.getTerminalService().scanner();
            sorting(method, tasks);
            for (Task task : tasks) {
                bootstrap.getTerminalService()
                        .outPutString(bootstrap.getToStringService().taskToString(task));
            }
        }
    }

    public void sorting(@NotNull final String method, @NotNull final List<Task> tasks) {
        switch (method) {
            case "by creating date":
                tasks.sort(Comparator.comparing(Task::getCreatingDate));
                break;
            case "by start date":
                tasks.sort(Comparator.comparing(Task::getStartDate));
                break;
            case "by end date":
                tasks.sort(Comparator.comparing(Task::getEndDate));
                break;
            case "by status":
                tasks.sort(Comparator.comparing(Task::getStatus));
                break;
            default:
                bootstrap.getTerminalService().outPutString("You have not selected a sorting method");
        }

    }
}
