package ru.shumov.tm.command;

import lombok.Getter;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Constants;
import ru.shumov.tm.dto.UserDTO;
import ru.shumov.tm.entity.User;
import ru.shumov.tm.enums.Role;

import java.io.File;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;

import static jakarta.xml.bind.JAXBContext.newInstance;

@Getter
public class UserRegistrationCommand extends AbstractCommand {
    private final String name = "registration";
    private final String description = "registration: Регистрация нового пользователя.";

    public void execute() throws NoSuchAlgorithmException {
        var userDTO = new UserDTO();
        @Nullable var user = bootstrap.getUser();
        if (user != null) {
            return;
        }
        bootstrap.getTerminalService().outPutString(Constants.ENTER_LOGIN);
        @NotNull final var login = bootstrap.getTerminalService().scanner();
        if (bootstrap.getUserService().getOne(login) != null) {
            bootstrap.getTerminalService().outPutString(Constants.USER_ALREADY_EXIST);
        } else {
            user = new User();
            bootstrap.getTerminalService().outPutString(Constants.ENTER_PASSWORD);
            @NotNull final var password = bootstrap.getTerminalService().scanner();
            if (password == null || password.isEmpty()) {
                bootstrap.getTerminalService().outPutString(Constants.INVALID_PASSWORD);
                return;
            }
            @NotNull final var output = bootstrap.getMd5Service().md5(password);
            user.setRole(Role.USER);
            user.setLogin(login);
            user.setPassword(output);
            bootstrap.getUserService().create(user);
            try {
                var jaxbContextProjectDTO =  newInstance(UserDTO.class);
                var marshallerUserDTO = jaxbContextProjectDTO.createMarshaller();
                marshallerUserDTO.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
                Collection<User> users = bootstrap.getUserService().getList();
                for(User obj : users) {
                    userDTO.getUsers().add(user);
                }
                marshallerUserDTO.marshal(userDTO, new File("C:\\Users\\bahrs\\IdeaProjects\\task-manager-10\\JAXBJsonUsers.json"));
            }
            catch (Exception e) {
                bootstrap.getTerminalService().outPutString("При регистрации произошла ошибка.");
                e.printStackTrace();
            }
            bootstrap.getTerminalService().outPutString(Constants.REGISTRATION_SUCCESSFUL);
        }
    }

    public UserRegistrationCommand() {
    }
}
