package ru.shumov.tm.command;

import lombok.Getter;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Constants;
import ru.shumov.tm.entity.Task;
import ru.shumov.tm.enums.Role;

import java.util.Collection;

public class TaskFindCommand extends AbstractCommand {
    private final Role role = Role.USER;
    @Getter
    private final String name = "find task";
    @Getter
    private final String description = "find task: Поиск задачи по названию или описанию.";

    @Override
    public void execute() {
        @Nullable final var user = bootstrap.getUser();
        if (user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if (user.getRole().equals(role)) {
            bootstrap.getTerminalService().outPutString("Введите название или описание задачи");
            final var answer = bootstrap.getTerminalService().scanner();
            Collection<Task> values = bootstrap.getTaskService().getList();
            for (Task task : values) {
                if (task.getName().contains(answer) || task.getDescription().contains(answer)) {
                    bootstrap.getTerminalService()
                            .outPutString(bootstrap.getToStringService().taskToString(task));
                }
            }
        }
    }
}
