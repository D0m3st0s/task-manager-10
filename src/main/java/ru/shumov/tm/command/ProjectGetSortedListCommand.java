package ru.shumov.tm.command;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Constants;
import ru.shumov.tm.entity.Project;
import ru.shumov.tm.enums.Role;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public class ProjectGetSortedListCommand extends AbstractCommand {

    private final Role role = Role.USER;
    @Getter
    private final String name = "project list sorted";
    @Getter
    private final String description = "project list sorted: Вывод всех проектов c сортировкой.";

    @Override
    public void execute() throws NoSuchAlgorithmException {
        List<Project> projects = new ArrayList<>();
        int counter = 0;
        @Nullable final var user = bootstrap.getUser();
        if (user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if (user.getRole().equals(role)) {
            @Nullable Collection<Project> values = bootstrap.getProjectService().getList();
            if (values.isEmpty()) {
                bootstrap.getTerminalService().outPutString(Constants.PROJECTS_DO_NOT_EXIST);
                return;
            }
            for (Project project : values) {
                if (project.getUserId().equals(bootstrap.getUser().getId())) {
                    counter++;
                    projects.add(project);
                }
            }
            if (counter == 0) {
                bootstrap.getTerminalService().outPutString(Constants.PROJECTS_DO_NOT_EXIST);
                return;
            }
            bootstrap.getTerminalService().outPutString("Как вы хотите отсортировать проекты:");
            bootstrap.getTerminalService().outPutString("by creating date/by start date/by end date/by status");
            var method = bootstrap.getTerminalService().scanner();
            sorting(method, projects);
            for (Project project : projects) {
                bootstrap.getTerminalService()
                        .outPutString(bootstrap.getToStringService().projectToString(project));
            }
        }
    }

    private void sorting(@NotNull final String method, @NotNull final List<Project> projects) {
        switch (method) {
            case "by creating date":
                projects.sort(Comparator.comparing(Project::getCreatingDate));
                break;
            case "by start date":
                projects.sort(Comparator.comparing(Project::getStartDate));
                break;
            case "by end date":
                projects.sort(Comparator.comparing(Project::getEndDate));
                break;
            case "by status":
                projects.sort(Comparator.comparing(Project::getStatus));
                break;
            default:
                bootstrap.getTerminalService().outPutString("You have not selected a sorting method");
        }
    }

}
