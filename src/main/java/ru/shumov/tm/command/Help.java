package ru.shumov.tm.command;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;

@Getter
public class Help extends AbstractCommand {
    private String name = "help";
    private String description = "help : Вывод доступных команд.";

    @Override
    public void execute() {
        @NotNull Collection<AbstractCommand> value = bootstrap.getCommandsService().getList();
        for (AbstractCommand abstractCommand : value) {
            bootstrap.getTerminalService().outPutString(abstractCommand.getDescription());
        }
    }

    public Help() {
    }
}
