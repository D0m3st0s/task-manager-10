package ru.shumov.tm.command;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Constants;
import ru.shumov.tm.entity.User;
import ru.shumov.tm.enums.Role;

public class ProjectGetOneCommand extends AbstractCommand {
    private final Role role = Role.USER;
    @Getter
    private final String name = "get project";
    @Getter
    private final String description = "get project: Вывод конкретного проекта.";

    @Override
    public void execute() {
        @Nullable final var user = bootstrap.getUser();
        if (user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if (user.getRole().equals(role)) {
            bootstrap.getTerminalService().outPutString(Constants.ENTER_PROJECT_ID);
            @NotNull final var projectId = bootstrap.getTerminalService().scanner();
            @NotNull final var project = bootstrap.getProjectService().getOne(projectId);
            if (project.getUserId().equals(user.getId())) {
                bootstrap.getTerminalService()
                        .outPutString(bootstrap.getToStringService().projectToString(project));
            } else {
                bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            }
        } else {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
        }
    }

    public ProjectGetOneCommand() {
    }
}
