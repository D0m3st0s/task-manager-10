package ru.shumov.tm.enums;

import lombok.Getter;

public enum Role {

    ADMINISTRATOR("Администратор"),
    USER("Обычный Пользователь");

    @Getter
    private String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }
}
