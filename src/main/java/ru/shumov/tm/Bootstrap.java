package ru.shumov.tm;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.persistence.jaxb.JAXBContext;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import ru.shumov.tm.command.AbstractCommand;
import ru.shumov.tm.dto.UserDTO;
import ru.shumov.tm.entity.User;
import ru.shumov.tm.exceptions.IncorrectInputException;
import ru.shumov.tm.repository.*;
import ru.shumov.tm.service.*;
import ru.shumov.tm.service.TerminalServiceImpl;
import ru.shumov.tm.service.Md5ServiceImpl;

import java.io.File;
import java.util.Set;

import static ru.shumov.tm.Constants.*;

public class Bootstrap implements ServiceLocator {

    @Getter
    private TerminalServiceImpl terminalService = new TerminalServiceImpl();
    @Getter
    private ConvertationServiceImpl conversionService = new ConvertationServiceImpl(this);
    @Getter
    private UnConvertationServiceImpl unConversionService = new UnConvertationServiceImpl(this);
    private TaskRepository taskRepository = new TaskRepositoryImpl();
    @Getter
    private TaskService taskService = new TaskServiceImpl(taskRepository, terminalService);
    private ProjectRepository projectRepository = new ProjectRepositoryImpl();
    @Getter
    private ProjectService projectService = new ProjectServiceImpl(projectRepository, terminalService, taskService);
    private UserRepository userRepository = new UserRepositoryImpl();
    @Getter
    private UserService userService = new UserServiceImpl(userRepository);
    @Getter
    private CommandsService commandsService = new CommandsServiceImpl(this);
    @Getter
    private Md5ServiceImpl md5Service = new Md5ServiceImpl();
    @Getter
    private ToStringServiceImpl toStringService = new ToStringServiceImpl();
    @Getter
    @Setter
    private User user = null;
    private boolean work = true;

    public void setWork(boolean work) {
        this.work = work;
    }

    public void init() {
        try {
            initCommand();
            usersLoad();
            start();
        } catch (Exception exception) {
            terminalService.outPutString("При запуске приложения возникла ошибка.");
            exception.printStackTrace();
        }
    }

    public void start() {
        terminalService.outPutString(WELCOME);
        terminalService.outPutString(PROJECT_ID);
        while (work) {
            try {
                String commandName = terminalService.scanner();
                if (commandsService.checkKey(commandName)) {
                    commandsService.commandExecute(commandName);
                } else {
                    terminalService.outPutString(COMMAND_DOES_NOT_EXIST);
                }
            } catch (Exception exception) {
                terminalService.outPutString("При работе приложения возникла ошибка.");
                exception.printStackTrace();
            }
        }
    }

    public void initCommand() {
        AbstractCommand command;

        final Set<Class<? extends AbstractCommand>> classes =
                new Reflections("ru.shumov.tm.command").getSubTypesOf(AbstractCommand.class);
        try {
            for (@NotNull final Class clazz : classes) {
                if (AbstractCommand.class.isAssignableFrom(clazz)) {
                    command = (AbstractCommand) clazz.newInstance();
                    regCommand(command);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void regCommand(AbstractCommand command) {
        @NotNull final var commandName = command.getName();
        @NotNull final var commandDescription = command.getDescription();
        try {
            if (commandName == null || commandName.isEmpty()) {
                throw new IncorrectInputException("Invalid Command Name: " + commandName);
            }
            if (commandDescription == null || commandDescription.isEmpty()) {
                throw new IncorrectInputException("Invalid Command Description" + commandDescription);
            }
            commandsService.commandAdd(command);
        } catch (IncorrectInputException e) {
            terminalService.outPutString(e.getMassage());
        }
    }

    public void usersLoad() {
        try {
            var jaxbContextProjectDTO =  JAXBContext.newInstance(UserDTO.class);
            var unMarshallerProjectDTO = jaxbContextProjectDTO.createUnmarshaller();
            unMarshallerProjectDTO.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
            var userDTO = (UserDTO) unMarshallerProjectDTO.unmarshal(new File("C:\\Users\\bahrs\\IdeaProjects\\task-manager-10\\JAXBJsonUsers.json"));
            var users = userDTO.getUsers();
            for (User obj : users) {
                getUserService().update(obj);
            }
        }
        catch (Exception e) {
            terminalService.outPutString("При загрузке данных пользователей произошла ошибка.");
            e.printStackTrace();
        }
    }
}
