package ru.shumov.tm.service;

import ru.shumov.tm.entity.Project;
import ru.shumov.tm.entity.Task;

public interface ToStringService {
    String projectToString(Project project);

    String taskToString(Task task);
}
