package ru.shumov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlAnnotationIntrospector;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import jakarta.xml.bind.Marshaller;
import org.eclipse.persistence.jaxb.JAXBContext;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.dto.ProjectDTO;
import ru.shumov.tm.dto.TaskDTO;
import ru.shumov.tm.entity.Project;
import ru.shumov.tm.entity.Task;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;

import static jakarta.xml.bind.JAXBContext.newInstance;

public class ConvertationServiceImpl implements ConvertationService {

    private Bootstrap bootstrap;

    public ConvertationServiceImpl(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void serialization() {
        var projectDTO = new ProjectDTO();
        var taskDTO = new TaskDTO();
        try {
            var fileOutputStreamProjects = new FileOutputStream("C:\\Users\\bahrs\\IdeaProjects\\task-manager-10\\projects.txt");
            var objectOutputStreamProjects = new ObjectOutputStream(fileOutputStreamProjects);

            var fileOutputStreamTasks = new FileOutputStream("C:\\Users\\bahrs\\IdeaProjects\\task-manager-10\\tasks.txt");
            var objectOutputStreamTasks = new ObjectOutputStream(fileOutputStreamTasks);

            Collection<Project> projects = bootstrap.getProjectService().getList();
            projectDTO.getProjects().addAll(projects);
            objectOutputStreamProjects.writeObject(projectDTO);
            objectOutputStreamProjects.close();
            fileOutputStreamProjects.close();

            Collection<Task> tasks = bootstrap.getTaskService().getList();
            taskDTO.getTasks().addAll(tasks);
            objectOutputStreamTasks.writeObject(taskDTO);
            fileOutputStreamTasks.close();
            objectOutputStreamTasks.close();
        } catch (Exception e) {
            bootstrap.getTerminalService().outPutString("Во время сохранения возникла ошибка.");
            e.printStackTrace();
        }
    }
    public void jaxbXmlConvertation() {
        try {
            var projectDTO = new ProjectDTO();
            var taskDTO = new TaskDTO();
            JAXBContext contextProjects = (JAXBContext) newInstance(ProjectDTO.class);
            var marshallerProjects = contextProjects.createMarshaller();
            marshallerProjects.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            var contextTasks = newInstance(TaskDTO.class);
            var marshallerTasks = contextTasks.createMarshaller();
            marshallerTasks.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            Collection<Project> projects = bootstrap.getProjectService().getList();
            projectDTO.getProjects().addAll(projects);
            marshallerProjects.marshal(projectDTO, new File("C:\\Users\\bahrs\\IdeaProjects\\task-manager-10\\JAXBXmlProjects.xml"));

            Collection<Task> tasks = bootstrap.getTaskService().getList();
            taskDTO.getTasks().addAll(tasks);
            marshallerTasks.marshal(taskDTO, new File("C:\\Users\\bahrs\\IdeaProjects\\task-manager-10\\JAXBXmlTasks.xml"));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void jaxbJsonConvertation() {
        try {
            var projectDTO = new ProjectDTO();
            var taskDTO = new TaskDTO();
            var jaxbContextProjectDTO =  newInstance(ProjectDTO.class);
            var marshallerProjectDTO = jaxbContextProjectDTO.createMarshaller();
            marshallerProjectDTO.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
            var jaxbContextTaskDTO =  newInstance(TaskDTO.class);
            var marshallerTaskDTO = jaxbContextTaskDTO.createMarshaller();
            marshallerTaskDTO.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");

            Collection<Project> projects = bootstrap.getProjectService().getList();
            projectDTO.getProjects().addAll(projects);
            marshallerProjectDTO.marshal(projectDTO, new File("C:\\Users\\bahrs\\IdeaProjects\\task-manager-10\\JAXBJsonProjects.json"));

            Collection<Task> tasks = bootstrap.getTaskService().getList();
            taskDTO.getTasks().addAll(tasks);
            marshallerTaskDTO.marshal(taskDTO, new File("C:\\Users\\bahrs\\IdeaProjects\\task-manager-10\\JAXBJsonTasks.json"));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void fasterJsonConvertation() {
        try {
            var projectDTO = new ProjectDTO();
            var taskDTO = new TaskDTO();
            var objectMapper = new ObjectMapper();
            var AnnotationIntrospector = new JacksonXmlAnnotationIntrospector();
            objectMapper.setAnnotationIntrospector(AnnotationIntrospector);

            Collection<Project> projects = bootstrap.getProjectService().getList();
            projectDTO.getProjects().addAll(projects);
            objectMapper.writeValue(new File("C:\\Users\\bahrs\\IdeaProjects\\task-manager-10\\fasterJsonProjects.json"), projectDTO);

            Collection<Task> tasks = bootstrap.getTaskService().getList();
            taskDTO.getTasks().addAll(tasks);
            objectMapper.writeValue(new File("C:\\Users\\bahrs\\IdeaProjects\\task-manager-10\\fasterJsonTasks.json"), taskDTO);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void fasterXmlConvertation() {
        try {
            var projectDTO = new ProjectDTO();
            var taskDTO = new TaskDTO();
            var xmlMapper = new XmlMapper();

            Collection<Project> projects = bootstrap.getProjectService().getList();
            projectDTO.getProjects().addAll(projects);
            xmlMapper.writeValue(new File("C:\\Users\\bahrs\\IdeaProjects\\task-manager-10\\fasterXmlProjects.xml"), projectDTO);

            Collection<Task> tasks = bootstrap.getTaskService().getList();
            taskDTO.getTasks().addAll(tasks);
            xmlMapper.writeValue(new File("C:\\Users\\bahrs\\IdeaProjects\\task-manager-10\\fasterXmlTasks.xml"), taskDTO);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
