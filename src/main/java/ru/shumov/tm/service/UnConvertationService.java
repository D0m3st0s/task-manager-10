package ru.shumov.tm.service;

public interface UnConvertationService {
    void unSerialization();

    void jaxbXmlUnConvertation();

    void fasterJsonUnConvertation();

    void jaxbJsonUnConvertation();

    void fasterXmlUnConvertation();
}
