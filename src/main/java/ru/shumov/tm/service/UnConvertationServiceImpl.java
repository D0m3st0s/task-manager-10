package ru.shumov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlAnnotationIntrospector;
import org.eclipse.persistence.jaxb.JAXBContext;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.dto.ProjectDTO;
import ru.shumov.tm.dto.TaskDTO;
import ru.shumov.tm.entity.Project;
import ru.shumov.tm.entity.Task;

import java.io.*;
import java.util.List;

public class UnConvertationServiceImpl {
    private Bootstrap bootstrap;

    public UnConvertationServiceImpl(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void unSerialization() {
        try {
            var fileInputStreamProjects = new FileInputStream("C:\\Users\\bahrs\\IdeaProjects\\task-manager-10\\projects.txt");
            var objectInputStreamProjects = new ObjectInputStream(fileInputStreamProjects);

            var fileInputStreamTasks = new FileInputStream("C:\\Users\\bahrs\\IdeaProjects\\task-manager-10\\tasks.txt");
            var objectInputStreamTasks = new ObjectInputStream(fileInputStreamTasks);

            var projectDTO = (ProjectDTO) objectInputStreamProjects.readObject();
            List<Project> projects = projectDTO.getProjects();
            for (Project project : projects) {
                bootstrap.getProjectService().update(project);
            }
            fileInputStreamProjects.close();
            objectInputStreamProjects.close();

            var taskDTO = (TaskDTO) objectInputStreamTasks.readObject();
            List<Task> tasks = taskDTO.getTasks();
            for (Task task : tasks) {
                bootstrap.getTaskService().update(task);
            }
            fileInputStreamTasks.close();
            objectInputStreamTasks.close();
        }
        catch (EOFException eofException) {

        }
        catch (Exception e) {
            bootstrap.getTerminalService().outPutString("При загрузке данных произошла ошибка.");
            e.printStackTrace();
        }
    }

    public void jaxbXmlUnConvertation() {
        try {
            var contextProjects = JAXBContext.newInstance(ProjectDTO.class);
            var unMarshallerProjects = contextProjects.createUnmarshaller();

            var contextTasks = JAXBContext.newInstance(TaskDTO.class);
            var unMarshallerTasks = contextTasks.createUnmarshaller();

            var projectDTO = (ProjectDTO) unMarshallerProjects.unmarshal(new File("C:\\Users\\bahrs\\IdeaProjects\\task-manager-10\\JAXBXmlProjects.xml"));
            var projects = projectDTO.getProjects();
            for (Project project : projects) {
                bootstrap.getProjectService().update(project);
            }

            var taskDTO = (TaskDTO) unMarshallerTasks.unmarshal(new File("C:\\Users\\bahrs\\IdeaProjects\\task-manager-10\\JAXBXmlTasks.xml"));
            var tasks = taskDTO.getTasks();
            for (Task task : tasks) {
                bootstrap.getTaskService().update(task);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fasterJsonUnConvertation() {
        try {
            var objectMapper = new ObjectMapper();
            var AnnotationIntrospector = new JacksonXmlAnnotationIntrospector();
            objectMapper.setAnnotationIntrospector(AnnotationIntrospector);

            var projectDTO = objectMapper.readValue(new File("C:\\Users\\bahrs\\IdeaProjects\\task-manager-10\\fasterJsonProjects.json"), ProjectDTO.class);
            var projects = projectDTO.getProjects();
            for (Project project : projects) {
                bootstrap.getProjectService().update(project);
            }

            var taskDTO = objectMapper.readValue(new File("C:\\Users\\bahrs\\IdeaProjects\\task-manager-10\\fasterJsonTasks.json"), TaskDTO.class);
            var tasks = taskDTO.getTasks();
            for (Task task : tasks) {
                bootstrap.getTaskService().update(task);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void jaxbJsonUnConvertation() {
        try {
            var jaxbContextProjectDTO =  JAXBContext.newInstance(ProjectDTO.class);
            var unMarshallerProjectDTO = jaxbContextProjectDTO.createUnmarshaller();
            unMarshallerProjectDTO.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
            var jaxbContextTaskDTO = JAXBContext.newInstance(TaskDTO.class);
            var unMarshallerTaskDTO = jaxbContextTaskDTO.createUnmarshaller();
            unMarshallerTaskDTO.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");

            var projectDTO = (ProjectDTO) unMarshallerProjectDTO.unmarshal(new File("C:\\Users\\bahrs\\IdeaProjects\\task-manager-10\\JAXBJsonProjects.json"));
            var projects = projectDTO.getProjects();
            for (Project project : projects) {
                bootstrap.getProjectService().update(project);
            }

            var taskDTO = (TaskDTO) unMarshallerTaskDTO.unmarshal(new File("C:\\Users\\bahrs\\IdeaProjects\\task-manager-10\\JAXBJsonTasks.json"));
            var tasks = taskDTO.getTasks();
            for (Task task : tasks) {
                bootstrap.getTaskService().update(task);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void fasterXmlUnConvertation() {
        try {
            var xmlMapper = new XmlMapper();

            var projectDTO = xmlMapper.readValue(new File("C:\\Users\\bahrs\\IdeaProjects\\task-manager-10\\fasterXmlProjects.xml"), ProjectDTO.class);
            var projects = projectDTO.getProjects();
            for (Project project : projects) {
                bootstrap.getProjectService().update(project);
            }

            var taskDTO = xmlMapper.readValue(new File("C:\\Users\\bahrs\\IdeaProjects\\task-manager-10\\fasterXmlTasks.xml"), TaskDTO.class);
            var tasks = taskDTO.getTasks();
            for (Task task : tasks) {
                bootstrap.getTaskService().update(task);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
