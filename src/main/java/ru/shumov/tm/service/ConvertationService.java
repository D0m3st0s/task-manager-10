package ru.shumov.tm.service;

public interface ConvertationService {
    void serialization();

    void jaxbXmlConvertation();

    void jaxbJsonConvertation();

    void fasterJsonConvertation();

    void fasterXmlConvertation();
}
