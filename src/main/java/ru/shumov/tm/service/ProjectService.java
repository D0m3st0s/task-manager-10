package ru.shumov.tm.service;

import ru.shumov.tm.entity.Project;

import java.util.Collection;

public interface ProjectService {

    void create(Project project);

    void update(Project project);

    void clear();

    void remove(String projectId);

    Collection<Project> getList();

    Project getOne(String id);

    boolean checkKey(String id);
}