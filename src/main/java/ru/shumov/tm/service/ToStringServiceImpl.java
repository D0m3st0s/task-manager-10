package ru.shumov.tm.service;

import ru.shumov.tm.entity.Project;
import ru.shumov.tm.entity.Task;

import java.text.SimpleDateFormat;

public class ToStringServiceImpl implements ToStringService{
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");

    public String projectToString(Project project) {
        return "Project{" +
                "id='" + project.getId() + '\'' +
                ", name='" + project.getName() + '\'' +
                ", description='" + project.getDescription() + '\'' +
                ", dateOfCreating=" + simpleDateFormat.format(project.getCreatingDate()) +
                ", startDate=" + simpleDateFormat.format(project.getStartDate()) +
                ", endDate=" + simpleDateFormat.format(project.getEndDate()) +
                ", status=" + project.getStatus() +
                '}';
    }

    public String taskToString(Task task) {
        return "Task{" +
                "id='" + task.getId() + '\'' +
                ", name='" + task.getName() + '\'' +
                ", description='" + task.getDescription() + '\'' +
                ", dateOfCreating=" + simpleDateFormat.format(task.getCreatingDate()) + '\'' +
                ", startDate=" + simpleDateFormat.format(task.getStartDate()) +
                ", endDate=" + simpleDateFormat.format(task.getEndDate()) +
                ", projectId='" + task.getProjectId() + '\'' +
                ", status='" + task.getStatus() + '\'' +
                '}';
    }
}
